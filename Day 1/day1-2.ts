import fs from 'fs'

const findFuelRecursive = (fuel: number, start: number = 0): number => {
  const reducedFuel = Math.floor(fuel / 3) - 2
  if (reducedFuel <= 0) return start
  start += reducedFuel
  return findFuelRecursive(reducedFuel, start)
}

const sumTotalFuel = () => {
  fs.readFile('./inputData.txt', 'utf-8', (err, data) => {
    try {
      if (err) throw err
      console.log(data
        .split('\n')
        .map(val => +val)
        .reduce((acc, value) => acc += findFuelRecursive(value), 0))
    } catch (err) {
      console.log(err)
    }
  })
}

sumTotalFuel()
