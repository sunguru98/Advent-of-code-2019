import fs from 'fs'

const findFuelSum = () => {
  try {
    fs.readFile('./inputData.txt', 'utf-8', (err, data) => {
      if (err) throw err
      const values = data.split('\n')
      console.log(values
        .map(value => parseInt(value))
        .reduce((acc, value) => (acc += Math.floor(value / 3) - 2), 0))
    })
  } catch (err) {
    console.log(err)
  }
}

findFuelSum()
