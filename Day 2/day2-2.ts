import fs from 'fs'

const computeOutput = (values: number[], noun: number, verb: number) => {
  let index = 0
  values[1] = noun
  values[2] = verb
  while (values[index] !== 99) {
    switch (values[index]) {
      case 1: {
        values[values[index + 3]] =
          values[values[index + 1]] + values[values[index + 2]]
        break
      }
      case 2: {
        values[values[index + 3]] =
          values[values[index + 1]] * values[values[index + 2]]
        break
      }
      default:
        null
    }
    index += 4
  }
  return { output: values[0], noun: values[1], verb: values[2] }
}

const findNounAndVerb = (values: number[], neededResult: number) => {
  for (let noun = 0; noun <= 99; noun++) {
    for (let verb = 0; verb <= 99; verb++) {
      const {
        output: result,
        noun: resNoun,
        verb: resVerb
      } = computeOutput([...values], noun, verb)
      if (result === neededResult) return 100 * resNoun + resVerb
    }
  }
  return null
}

const runCode = () =>
  new Promise((resolve, reject) => {
    try {
      fs.readFile('./inputData.txt', 'utf-8', (err, data) => {
        if (err) throw err
        const values = data.split(',').map(values => +values)
        const neededResult = 19690720
        const result = findNounAndVerb(values, neededResult)
        if (!result)
          resolve('Nothing found')
        resolve(result)
      })
    } catch (err) {
      reject(err)
    }
  })

runCode()
  .then(res => console.log(res))
  .catch(err => console.log(err))
