import fs from 'fs'

const restoreGravity = () => {
  fs.readFile('./inputData.txt', 'utf-8', (err, data) => {
    try {
      if (err) throw err
      const values = data.split(',').map(value => +value)
      let index = 0
      values[1] = 12
      values[2] = 2
      while (values[index] !== 99) {
        switch (values[index]) {
          case 1: {
            values[values[index + 3]] =
              values[values[index + 1]] + values[values[index + 2]]
            break
          }
          case 2: {
            values[values[index + 3]] =
              values[values[index + 1]] * values[values[index + 2]]
            break
          }
          default:
            null
        }
        index += 4
      }
      console.log(values[0])
    } catch (err) {
      console.log(err)
    }
  })
}

restoreGravity()
